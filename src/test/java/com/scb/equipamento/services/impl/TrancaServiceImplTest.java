package com.scb.equipamento.services.impl;

import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.NovaTranca;
import com.scb.equipamento.models.enums.Status;
import com.scb.equipamento.repositories.BicicletaRepository;
import com.scb.equipamento.repositories.TotemRepository;
import com.scb.equipamento.repositories.TrancaRepository;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest
class TrancaServiceImplTest {
    private static final Long ID = Long.MIN_VALUE;
    private static final String LOCALIZACAO = "41.40340, 2.17405";
    private static final Integer INDEX = 0;
    private static final Year ANO_DE_FABRICACAO = Year.of(2015);
    private static final String MODELO = "T-99";
    private static final Status STATUS = Status.NOVA;

    @InjectMocks
    private TrancaServiceImpl trancaService;
    @Mock
    private TrancaRepository trancaRepository;
    private Tranca tranca;
    private NovaTranca novaTranca;
    private Optional<Tranca> optionalTranca;
    @Mock
    private ModelMapper mapper;
    @Mock
    private TotemRepository totemRepository;
    @Autowired
    private BicicletaRepository bicicletaRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        inicializaTranca();
    }

    @Test
    void whenRecuperaTrancasThenReturnAnListOfTranca() {
        when(trancaRepository.findAll()).thenReturn(List.of(tranca));

        List<Tranca> response = trancaService.recuperaTrancas();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(Tranca.class, response.get(INDEX).getClass());
        assertEquals(ID, response.get(INDEX).getId());
        assertEquals(LOCALIZACAO, response.get(INDEX).getLocalizacao());
        assertEquals(ANO_DE_FABRICACAO, response.get(INDEX).getAnoDeFabricacao());
        assertEquals(MODELO, response.get(INDEX).getModelo());
        assertEquals(STATUS, response.get(INDEX).getStatus());
    }

    @Test
    void whenRecuperaTrancaThenReturnAnTrancaInstance() {
        when(trancaRepository.findById(anyLong())).thenReturn(optionalTranca);

        Tranca response = trancaService.recuperaTranca(ID);

        assertNotNull(response);
        assertEquals(Tranca.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(LOCALIZACAO, response.getLocalizacao());
        assertEquals(ANO_DE_FABRICACAO, response.getAnoDeFabricacao());
        assertEquals(MODELO, response.getModelo());
        assertEquals(STATUS, response.getStatus());
    }

    @Test
    void whenRecuperaTrancaThenReturnAnObjectNotFoundException() {
        when(trancaRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            trancaService.recuperaTranca(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    @Test
    void whenCadastraTrancaThenReturnSuccess() {
        when(trancaRepository.save(any())).thenReturn(tranca);

        Tranca response = trancaService.cadastraTranca(novaTranca);

        assertNotNull(response);
        assertEquals(Tranca.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(LOCALIZACAO, response.getLocalizacao());
        assertEquals(ANO_DE_FABRICACAO, response.getAnoDeFabricacao());
        assertEquals(MODELO, response.getModelo());
        assertEquals(STATUS, response.getStatus());
    }

    @Test
    void whenAtualizaTrancaThenReturnSuccess() {
        when(trancaRepository.save(any())).thenReturn(tranca);
        when(trancaRepository.findById(anyLong())).thenReturn(optionalTranca);

        Tranca response = trancaService.atualizaTranca(novaTranca);

        assertNotNull(response);
        assertEquals(Tranca.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(LOCALIZACAO, response.getLocalizacao());
        assertEquals(ANO_DE_FABRICACAO, response.getAnoDeFabricacao());
        assertEquals(MODELO, response.getModelo());
        assertEquals(STATUS, response.getStatus());
    }

    @Test
    void whenAtualizaTrancaThenReturnAnObjectNotFoundException() {
        when(trancaRepository.findById(anyLong())).thenReturn(Optional.empty());

        try{
            trancaService.recuperaTranca(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    @Test
    void whenDeletaTrancaThenReturnSuccess() {
        when(trancaRepository.findById(anyLong())).thenReturn(optionalTranca);

        doNothing().when(trancaRepository).delete(any());
        trancaService.deletaTranca(ID);
        verify(trancaRepository, times(1)).delete(any());
    }

    @Test
    void whenDeletaTrancaThenReturnAnObjectNotFoundException() {
        when(trancaRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            trancaService.deletaTranca(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

//    @Test
//    void whenIntegrarNaRedeDeTotensThenReturnSuccess() {
//        final Long ID_TRANCA = 55L;
//        final Long ID_TOTEM = 55L;
//
//        Tranca trancaExemplo = tranca;
//        trancaExemplo.setId(ID_TRANCA);
//        Totem totemExemplo = new Totem(ID_TOTEM, LOCALIZACAO, new ArrayList<>());
//
//        when(trancaRepository.findById(anyLong())).thenReturn(Optional.of(trancaExemplo));
//        when(totemRepository.findById(anyLong())).thenReturn(Optional.of(totemExemplo));
//
//        trancaService.integrarNaRedeDeTotens(new IntegraTrancaDto(ID_TOTEM, ID_TRANCA));
//
//        assertEquals(trancaExemplo.getTotem(), totemExemplo);
//        assertEquals(totemExemplo.getTrancas().get(INDEX), trancaExemplo);
//    }

//    @Test
//    void whenTrancarThenReturnSuccess() {
//        Tranca trancaExemplo = tranca;
//        Bicicleta bicicletaExemplo = new Bicicleta(ID, "Marca", "Modelo", Year.now(), Status.NOVA);
//        when(trancaRepository.findById(anyLong())).thenReturn(Optional.of(trancaExemplo));
//        trancaExemplo.setStatus(Status.DISPONIVEL);
//        when(bicicletaRepository.findById(anyLong())).thenReturn(Optional.of(bicicletaExemplo));
//
//        trancaService.trancar(trancaExemplo, bicicletaExemplo);
//
//        assertEquals(trancaExemplo.getBicicleta(), bicicletaExemplo);
//        assertEquals(bicicletaExemplo.getStatus(), Status.DISPONIVEL);
//    }

    private void inicializaTranca() {
        tranca = new Tranca(ID, LOCALIZACAO, ANO_DE_FABRICACAO, MODELO, Status.NOVA, null, null);
        novaTranca = new NovaTranca(ID, LOCALIZACAO, ANO_DE_FABRICACAO, MODELO, Status.NOVA);
        optionalTranca = Optional.of(new Tranca(ID, LOCALIZACAO, ANO_DE_FABRICACAO, MODELO, STATUS, null, null));
    }
}
