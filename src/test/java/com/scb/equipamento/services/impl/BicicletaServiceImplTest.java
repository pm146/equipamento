package com.scb.equipamento.services.impl;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.dtos.NovaBicicleta;
import com.scb.equipamento.models.enums.Status;
import com.scb.equipamento.repositories.BicicletaRepository;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@SpringBootTest
class BicicletaServiceImplTest {
    private static final Long ID = Long.MIN_VALUE;
    private static final Integer INDEX = 0;
    private static final String MODELO = "T-99";
    private static final String MARCA = "T-99";
    private static final Year ANO = Year.of(2015);
    private static final Status STATUS = Status.NOVA;

    @InjectMocks
    private BicicletaServiceImpl bicicletaService;
    @Mock
    private BicicletaRepository bicicletaRepository;
    private Bicicleta bicicleta;
    private NovaBicicleta novaBicicleta;
    private Optional<Bicicleta> optionalBicicleta;
    @Mock
    private ModelMapper mapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        inicializaBicicleta();
    }

    @Test
    void whenRecuperaBicicletasThenReturnAnListOfBicicleta() {
        when(bicicletaRepository.findAll()).thenReturn(List.of(bicicleta));


        List<Bicicleta> response = bicicletaService.recuperaBicicletas();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(Bicicleta.class, response.get(INDEX).getClass());
        assertEquals(ID, response.get(INDEX).getId());
        assertEquals(MARCA, response.get(INDEX).getMarca());
        assertEquals(MODELO, response.get(INDEX).getModelo());
        assertEquals(ANO, response.get(INDEX).getAno());
        assertEquals(STATUS, response.get(INDEX).getStatus());
    }

    @Test
    void whenRecuperaBicicletaThenReturnAnBicicletaInstance() {
        when(bicicletaRepository.findById(anyLong())).thenReturn(optionalBicicleta);

        Bicicleta response = bicicletaService.recuperaBicicleta(ID);

        assertNotNull(response);
        assertEquals(Bicicleta.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(MARCA, response.getMarca());
        assertEquals(ANO, response.getAno());
        assertEquals(MODELO, response.getModelo());
        assertEquals(STATUS, response.getStatus());
    }

    @Test
    void whenRecuperaBicicletaThenReturnAnObjectNotFoundException() {
        when(bicicletaRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            bicicletaService.recuperaBicicleta(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    @Test
    void whenCadastraBicicletaThenReturnSuccess() {
        when(bicicletaRepository.save(any())).thenReturn(bicicleta);

        Bicicleta response = bicicletaService.cadastraBicicleta(novaBicicleta);

        assertNotNull(response);
        assertEquals(Bicicleta.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(MARCA, response.getMarca());
        assertEquals(ANO, response.getAno());
        assertEquals(MODELO, response.getModelo());
        assertEquals(STATUS, response.getStatus());
    }

    @Test
    void whenAtualizaBicicletaThenReturnSuccess() {
        when(bicicletaRepository.save(any())).thenReturn(bicicleta);
        when(bicicletaRepository.findById(anyLong())).thenReturn(optionalBicicleta);

        Bicicleta response = bicicletaService.atualizaBicicleta(novaBicicleta);

        assertEquals(ID, response.getId());
        assertEquals(MARCA, response.getMarca());
        assertEquals(MODELO, response.getModelo());
        assertEquals(ANO, response.getAno());
        assertEquals(STATUS, response.getStatus());
    }

    @Test
    void whenAtualizaBicicletaThenReturnAnObjectNotFoundException() {
        when(bicicletaRepository.findById(anyLong())).thenReturn(Optional.empty());

        try{
            bicicletaService.recuperaBicicleta(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    @Test
    void whenDeletaBicicletaThenReturnSuccess() {
        Bicicleta bicicletaARemover = new Bicicleta(ID, MARCA, MODELO, ANO, Status.APOSENTADA);

        when(bicicletaRepository.findById(anyLong())).thenReturn(Optional.of(bicicletaARemover));

        doNothing().when(bicicletaRepository).delete(any());
        bicicletaService.deletaBicicleta(ID);
        verify(bicicletaRepository, times(1)).delete(any());
    }

    @Test
    void whenDeletaBicicletaThenReturnAnObjectNotFoundException() {
        when(bicicletaRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            bicicletaService.deletaBicicleta(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    private void inicializaBicicleta() {
        bicicleta = new Bicicleta(ID, MARCA, MODELO, ANO, STATUS);
        novaBicicleta = new NovaBicicleta(ID, MARCA, MODELO, ANO, STATUS);
        optionalBicicleta = Optional.of(new Bicicleta(ID, MARCA, MODELO, ANO, STATUS));
    }
}
