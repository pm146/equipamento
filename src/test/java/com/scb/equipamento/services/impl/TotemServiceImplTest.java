package com.scb.equipamento.services.impl;

import com.scb.equipamento.models.Totem;
import com.scb.equipamento.models.dtos.NovoTotem;
import com.scb.equipamento.repositories.TotemRepository;
import com.scb.equipamento.services.exceptions.DataIntegratyViolationException;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class TotemServiceImplTest {

    private static final Long ID = Long.MIN_VALUE;
    private static final String LOCALIZACAO = "41.40340, 2.17405";
    private static final Integer INDEX = 0;
    private static final String LOCALIZACAO_JA_CADASTRADA_NO_SISTEMA = "Localização já cadastrada no sistema.";

    @InjectMocks
    private TotemServiceImpl totemService;
    @Mock
    private TotemRepository totemRepository;
    @Mock
    private ModelMapper mapper;
    private Totem totem;
    private NovoTotem novoTotem;
    private Optional<Totem> optionalTotem;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        inicializaTotem();
    }

    @Test
    void whenRecuperaTotensThenReturnAnListOfTotem() {
        when(totemRepository.findAll()).thenReturn(List.of(totem));

        List<Totem> response = totemService.recuperaTotens();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(Totem.class, response.get(INDEX).getClass());
        assertEquals(ID, response.get(INDEX).getId());
        assertEquals(LOCALIZACAO, response.get(INDEX).getLocalizacao());
    }

    @Test
    void whenCadastraTotemThenReturnSuccess() {
        when(totemRepository.save(any())).thenReturn(totem);

        Totem response = totemService.cadastraTotem(novoTotem);

        assertNotNull(response);
        assertEquals(Totem.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(LOCALIZACAO, response.getLocalizacao());
    }

    @Test
    void whenCadastraTotemThenReturnAnDataIntegrityViolationException() {
        when(totemRepository.findByLocalizacao(anyString())).thenReturn(optionalTotem);

        try {
            optionalTotem.get().setId(8L);
            totemService.cadastraTotem(novoTotem);
        } catch (Exception ex) {
            assertEquals(DataIntegratyViolationException.class, ex.getClass());

        }
    }

    @Test
    void whenAtualizaTotemThenReturnSuccess() {
        when(totemRepository.save(any())).thenReturn(totem);
        when(totemRepository.findById(anyLong())).thenReturn(optionalTotem);

        Totem response = totemService.atualizaTotem(novoTotem);

        assertNotNull(response);
        assertEquals(Totem.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(LOCALIZACAO, response.getLocalizacao());
    }

    @Test
    void whenAtualizaTotemThenReturnAnDataIntegrityViolationException() {
        when(totemRepository.findByLocalizacao(anyString())).thenReturn(optionalTotem);

        try {
            optionalTotem.get().setId(9L);
            totemService.cadastraTotem(novoTotem);
        } catch (Exception ex) {
            assertEquals(DataIntegratyViolationException.class, ex.getClass());
            assertEquals(LOCALIZACAO_JA_CADASTRADA_NO_SISTEMA, ex.getMessage());
        }
    }

    @Test
    void whenDeletaTotemThenReturnSuccess() {
        when(totemRepository.findById(anyLong())).thenReturn(optionalTotem);

        doNothing().when(totemRepository).delete(any());
        totemService.deletaTotem(ID);
        verify(totemRepository, times(1)).delete(any());
    }

    @Test
    void whenDeletaTotemThenReturnObjectNotFoundException() {
        when(totemRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            totemService.deletaTotem(ID);
        } catch (Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    @Test
    void whenRecuperaTotemThenReturnAnTotemInstance() {
        when(totemRepository.findById(anyLong())).thenReturn(optionalTotem);
        Totem response = totemService.recuperaTotem(ID);

        assertNotNull(response);
        assertEquals(Totem.class, response.getClass());
        assertEquals(ID, response.getId());
        assertEquals(LOCALIZACAO, response.getLocalizacao());
    }

    @Test
    void whenRecuperaTotemThenReturnAnObjectNotFoundException() {
        when(totemRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            totemService.recuperaTotem(ID);
        } catch(Exception ex) {
            assertEquals(ObjectNotFoundException.class, ex.getClass());
        }
    }

    private void inicializaTotem(){
        totem = new Totem(ID, LOCALIZACAO,null);
        novoTotem = new NovoTotem(ID, LOCALIZACAO);
        optionalTotem = Optional.of(new Totem(ID, LOCALIZACAO, null));
    }
}