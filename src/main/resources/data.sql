INSERT INTO totem (id, localizacao) VALUES (1, '41.40338, 2.17403');
INSERT INTO totem (id, localizacao) VALUES (2, '41.40339, 2.17404');

INSERT INTO tranca (id, localizacao, ano_de_fabricacao, modelo, status) VALUES (1, '41.40338, 2.17403', 2015, 'T-01', 2);
INSERT INTO tranca (id, localizacao, ano_de_fabricacao, modelo, status) VALUES (2, '41.40339, 2.17404', 1996, 'T-02', 2);

INSERT INTO bicicleta (id, marca, modelo, ano, status) VALUES (1, 'X-CROSS', 'New Venture 5D', 2019, 2);
INSERT INTO bicicleta (id, marca, modelo, ano, status) VALUES (2, 'Oklahoma', 'Classic', 1998, 2);