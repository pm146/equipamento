package com.scb.equipamento.repositories;

import com.scb.equipamento.models.Totem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TotemRepository extends JpaRepository<Totem, Long> {
    Optional<Totem> findByLocalizacao(String localizacao);
}
