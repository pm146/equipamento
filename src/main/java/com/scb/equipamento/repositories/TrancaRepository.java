package com.scb.equipamento.repositories;

import com.scb.equipamento.models.Tranca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrancaRepository extends JpaRepository<Tranca, Long> {
}
