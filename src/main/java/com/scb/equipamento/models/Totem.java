package com.scb.equipamento.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "totem")
public class Totem {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String localizacao;
    @JsonIgnore
    @OneToMany
    private List<Tranca> trancas = new ArrayList<>();
}
