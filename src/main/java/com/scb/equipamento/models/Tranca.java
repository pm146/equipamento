package com.scb.equipamento.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scb.equipamento.models.enums.Status;
import jakarta.persistence.*;
import lombok.*;

import java.time.Year;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tranca")
public class Tranca {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String localizacao;

    @Column(name = "anoDeFabricacao")
    private Year anoDeFabricacao;
    private String modelo;
    private Status status = Status.NOVA;
    @JsonIgnore
    @ManyToOne
    private Totem totem;
    @OneToOne
    @JoinColumn(name = "bicicleta_id")
    private Bicicleta bicicleta;
}