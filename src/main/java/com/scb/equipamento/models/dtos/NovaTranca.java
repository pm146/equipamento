package com.scb.equipamento.models.dtos;

import com.scb.equipamento.models.enums.Status;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Year;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class NovaTranca {
    private Long id;
    @NotBlank
    private String localizacao;
    @NotNull
    private Year anoDeFabricacao;
    @NotBlank
    private String modelo;
    private Status status = Status.NOVA;
}
