package com.scb.equipamento.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class IntegraTrancaDto {
    private Long idTotem;
    private Long idTranca;
}
