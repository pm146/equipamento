package com.scb.equipamento.models.enums;

import com.scb.equipamento.services.exceptions.DataIntegratyViolationException;

public enum Status {
    DISPONIVEL, EM_USO, NOVA, APOSENTADA, REPARO_SOLICITADO, EM_REPARO;

    public static Status selecionaStatus(String acao) {
        return switch (acao) {
            case "DISPONIVEL" -> Status.DISPONIVEL;
            case "EM_USO" -> Status.EM_USO;
            case "NOVA" -> Status.NOVA;
            case "APOSENTADA" -> Status.APOSENTADA;
            case "REPARO_SOLICITADO" -> Status.REPARO_SOLICITADO;
            case "EM_REPARO" -> Status.EM_REPARO;
            default -> throw new DataIntegratyViolationException("Status inválido");
        };
    }
}