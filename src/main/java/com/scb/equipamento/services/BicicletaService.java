package com.scb.equipamento.services;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.dtos.NovaBicicleta;
import com.scb.equipamento.models.dtos.IntegraBicicletaDto;

import java.util.List;

public interface BicicletaService {
    List<Bicicleta> recuperaBicicletas();
    Bicicleta recuperaBicicleta(Long id);
    Bicicleta cadastraBicicleta(NovaBicicleta novaBicicleta);
    Bicicleta atualizaBicicleta(NovaBicicleta novaBicicleta);
    void deletaBicicleta(Long id);
    void integrarNaRedeDeTotens(IntegraBicicletaDto integraBicicletaDto);
    void retirarDaRedeDeTotens(IntegraBicicletaDto integraBicicletaDto);
    Bicicleta mudarStatus(Long id, String acao);
}
