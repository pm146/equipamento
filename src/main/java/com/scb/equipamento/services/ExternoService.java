package com.scb.equipamento.services;

public interface ExternoService {
    void enviarEmail(String email, String mensagem);
    String getEmailFuncionario(Long idFuncionario);
}
