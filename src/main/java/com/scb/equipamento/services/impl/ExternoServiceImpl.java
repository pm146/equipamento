package com.scb.equipamento.services.impl;

import com.scb.equipamento.services.ExternoService;
import com.scb.equipamento.services.exceptions.DataIntegratyViolationException;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class ExternoServiceImpl implements ExternoService {

    private static final String API_EXTERNO_EMAIL = "https://bicicletario.herokuapp.com/api/externo/email/enviarEmail";
    private static final String API_ALUGUEL_FUNCIONARIO = "https://aluguel-production.up.railway.app/funcionario/";

    @Override
    public void enviarEmail(String email, String mensagem) {
        HttpRequest request;

        try {
            request = HttpRequest.newBuilder()
                    .uri(URI.create(API_EXTERNO_EMAIL))
                    .POST(HttpRequest.BodyPublishers.ofString(montarJsonEmail(email, mensagem)))
                    .headers("Content-Type", "application/json")
                    .build();
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new DataIntegratyViolationException("Deu ruim na hora de montar a requisição. " + e.getMessage());
        }

        HttpClient httpClient = HttpClient.newBuilder().build();

        HttpResponse<String> response;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            if(response.statusCode() != 200)
                throw new DataIntegratyViolationException("Erro ao enviar email");

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            throw new DataIntegratyViolationException("Deu ruim na hora de enviar. " + e.getMessage());
        }
    }

    private String montarJsonEmail(String email, String mensagem) {
        String jsonEmail = """
            {"email": "%s", "mensagem": "%s"}
        """;

        return String.format(jsonEmail, email, mensagem);
    }

    @Override
    public String getEmailFuncionario(Long idFuncionario) {
        HttpRequest request;

        try {
            request = HttpRequest.newBuilder()
                    .uri(URI.create(API_ALUGUEL_FUNCIONARIO + idFuncionario))
                    .GET()
                    .build();
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new DataIntegratyViolationException("Deu ruim na hora de montar a requisição. " + e.getMessage());
        }

        HttpClient httpClient = HttpClient.newBuilder().build();

        HttpResponse<String> response;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 404)
                throw new ObjectNotFoundException("Funcionario não encontrado");
            else if (response.statusCode() == 422)
                throw new DataIntegratyViolationException("Dados de funcionário inválidos");

            JSONObject funcionario = new JSONObject(response.body());
            return funcionario.get("email").toString();

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            throw new DataIntegratyViolationException("Deu ruim na hora de enviar. " + e.getMessage());
        }

        return null;
    }
}
