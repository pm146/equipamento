package com.scb.equipamento.services.impl;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Totem;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.IntegraTrancaDto;
import com.scb.equipamento.models.dtos.TrancaBicicletaDto;
import com.scb.equipamento.models.dtos.NovaTranca;
import com.scb.equipamento.models.enums.Status;
import com.scb.equipamento.repositories.TotemRepository;
import com.scb.equipamento.repositories.TrancaRepository;
import com.scb.equipamento.services.BicicletaService;
import com.scb.equipamento.services.ExternoService;
import com.scb.equipamento.services.TotemService;
import com.scb.equipamento.services.TrancaService;
import com.scb.equipamento.services.exceptions.DataIntegratyViolationException;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrancaServiceImpl implements TrancaService {

    @Autowired
    private ModelMapper mapper;
    @Autowired
    private TrancaRepository trancaRepository;
    @Autowired
    private TotemRepository totemRepository;
    @Autowired
    private TotemService totemService;
    @Autowired @Lazy
    private BicicletaService bicicletaService;
    @Autowired
    private ExternoService externoService;

    @Override
    public List<Tranca> recuperaTrancas() {
        return trancaRepository.findAll();
    }

    @Override
    public Tranca recuperaTranca(Long id) {
        Optional<Tranca> tranca = trancaRepository.findById(id);
        return tranca.orElseThrow( () -> new ObjectNotFoundException("Tranca não encontrada"));
    }

    @Override
    public Tranca cadastraTranca(NovaTranca novaTranca) {
        return trancaRepository.save(mapper.map(novaTranca, Tranca.class));
    }

    @Override
    public Tranca atualizaTranca(NovaTranca novaTranca) {
        recuperaTranca(novaTranca.getId());
        return trancaRepository.save(mapper.map(novaTranca, Tranca.class));
    }

    @Override
    public void deletaTranca(Long id) {
        Tranca tranca = recuperaTranca(id);
        if(tranca.getBicicleta() != null)
            throw new DataIntegratyViolationException("Tranca contém bicicleta. Não pode ser removida.");
        trancaRepository.delete(tranca);
    }

    @Override
    public void integrarNaRedeDeTotens(IntegraTrancaDto integraTrancaDto) {

        Tranca tranca = recuperaTranca(integraTrancaDto.getIdTranca());

        Status statusTranca = tranca.getStatus();
        if( ! (statusTranca == Status.NOVA || statusTranca == Status.EM_REPARO) )
            throw new DataIntegratyViolationException("Tranca com status inválido.");

        Totem totem = totemService.recuperaTotem(integraTrancaDto.getIdTotem());

        adicionarTrancaAoTotem(tranca, totem);
    }

    public void adicionarTrancaAoTotem(Tranca tranca, Totem totem){
        List<Tranca> listaTrancas = totem.getTrancas();

        if(listaTrancas.size() > TotemService.MAX_TRANCAS)
            throw new DataIntegratyViolationException("Número máximo de trancas atingido");

        if(listaTrancas.contains(tranca))
            throw new DataIntegratyViolationException("Tranca já registrada no totem");

        // Enviar email
        final Long idFuncionario = 1L;
        String emailFuncionario = externoService.getEmailFuncionario(idFuncionario);
        String mensagem = mensagemEmailTrancaIntegrada(tranca, totem);
        externoService.enviarEmail(emailFuncionario, mensagem);

        listaTrancas.add(tranca);
        totem.setTrancas(listaTrancas);
        tranca.setTotem(totem);
        tranca.setStatus(Status.DISPONIVEL);

        trancaRepository.saveAndFlush(tranca);
        totemRepository.saveAndFlush(totem);
    }

    private String mensagemEmailTrancaIntegrada(Tranca tranca, Totem totem) {
        return "A tranca de id '"+ tranca.getId() +"' foi integrada ao totem de id '"+ totem.getId() +"' com sucesso.";
    }

    @Override
    public void retirarDaRedeDeTotens(IntegraTrancaDto integraTrancaDto) {
        Tranca tranca = recuperaTranca(integraTrancaDto.getIdTranca());

        Totem totem = totemService.recuperaTotem(integraTrancaDto.getIdTotem());

        // Ainda não sei como detectar se o cliente quer retirar a tranca em reparo ou aposentadoria
        if(tranca.getStatus() != Status.REPARO_SOLICITADO)
            throw new DataIntegratyViolationException("Status de tranca inválido para operação");

        removerTrancaDoTotem(tranca, totem);
    }

    public void removerTrancaDoTotem(Tranca tranca, Totem totem){
        List<Tranca> listaTrancas = totem.getTrancas();

        if(!listaTrancas.contains(tranca))
            throw new DataIntegratyViolationException("Tranca não registrada no totem");

        // Enviar email
        final Long idFuncionario = 1L;
        String emailFuncionario = externoService.getEmailFuncionario(idFuncionario);
        String mensagem = mensagemEmailTrancaRetirada(tranca, totem);
        externoService.enviarEmail(emailFuncionario, mensagem);

        listaTrancas.remove(tranca);
        totem.setTrancas(listaTrancas);
        tranca.setTotem(null);
        tranca.setStatus(Status.EM_REPARO);

        totemRepository.saveAndFlush(totem);
        trancaRepository.saveAndFlush(tranca);
    }

    private String mensagemEmailTrancaRetirada(Tranca tranca, Totem totem) {
        return "A tranca de id '"+ tranca.getId() +"' foi retirada do totem de id '"+ totem.getId() +"' com sucesso.";
    }

    @Override
    public Bicicleta recuperaBicicletaEmTranca(Long id) {
        Tranca tranca = recuperaTranca(id);
        return tranca.getBicicleta();
    }

    @Override
    public void trancar(Tranca tranca, Bicicleta bicicleta) {
        if(tranca.getTotem() == null)
            throw new DataIntegratyViolationException("Tranca sem associação a um totem");
        else if(tranca.getStatus() != Status.DISPONIVEL)
            throw new DataIntegratyViolationException("Tranca indisponível");

        if(bicicleta != null)
            tranca.setBicicleta(bicicleta);
        tranca.setStatus(Status.EM_USO);

        trancaRepository.saveAndFlush(tranca);
    }

    @Override
    public Tranca trancar(Long id, TrancaBicicletaDto trancaBicicletaDto) {
        Tranca tranca = recuperaTranca(id);
        Long idBicicleta = trancaBicicletaDto.getIdBicicleta();

        Bicicleta bicicleta = null;
        if (idBicicleta != null) {
            bicicleta = bicicletaService.recuperaBicicleta(idBicicleta);
            bicicletaService.mudarStatus(idBicicleta, Status.DISPONIVEL.name());
        }
        trancar(tranca, bicicleta);

        return tranca;
    }

    @Override
    public Bicicleta destrancar(Tranca tranca) {
        if(tranca.getStatus() != Status.EM_USO)
            throw new DataIntegratyViolationException("Tranca em estado inválido");

        Bicicleta bicicleta = tranca.getBicicleta();
        tranca.setBicicleta(null);
        tranca.setStatus(Status.DISPONIVEL);

        trancaRepository.saveAndFlush(tranca);
        return bicicleta;
    }

    @Override
    public Tranca destrancar(Long id, TrancaBicicletaDto trancaBicicletaDto) {
        Tranca tranca = recuperaTranca(id);
        Long idBicicleta = trancaBicicletaDto.getIdBicicleta();

        destrancar(tranca);

        if(idBicicleta != null)
            bicicletaService.mudarStatus(idBicicleta, Status.EM_USO.name());

        return tranca;
    }

    @Override
    public Tranca mudarStatus(Long id, String acao) {
        Tranca tranca = recuperaTranca(id);
        if(acao.equals("TRANCAR"))
            trancar(tranca, null);
        else if(acao.equals("DESTRANCAR"))
            destrancar(tranca);
        else
            throw new DataIntegratyViolationException("Status inválido");

        trancaRepository.saveAndFlush(tranca);
        return tranca;
    }
}
