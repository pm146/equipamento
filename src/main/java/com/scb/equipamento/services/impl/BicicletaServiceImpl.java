package com.scb.equipamento.services.impl;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.NovaBicicleta;
import com.scb.equipamento.models.dtos.IntegraBicicletaDto;
import com.scb.equipamento.models.enums.Status;
import com.scb.equipamento.repositories.BicicletaRepository;
import com.scb.equipamento.services.BicicletaService;
import com.scb.equipamento.services.ExternoService;
import com.scb.equipamento.services.TrancaService;
import com.scb.equipamento.services.exceptions.DataIntegratyViolationException;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BicicletaServiceImpl implements BicicletaService {

    @Autowired
    private ModelMapper mapper;
    @Autowired
    private BicicletaRepository bicicletaRepository;
    @Autowired
    private TrancaService trancaService;
    @Autowired
    private ExternoService externoService;


    @Override
    public List<Bicicleta> recuperaBicicletas() { return bicicletaRepository.findAll(); }

    @Override
    public Bicicleta recuperaBicicleta(Long id) {
        Optional<Bicicleta> bicicleta = bicicletaRepository.findById(id);
        return bicicleta.orElseThrow( () -> new ObjectNotFoundException("Bicicleta não encontrada"));
    }

    @Override
    public Bicicleta cadastraBicicleta(NovaBicicleta novaBicicleta) {
        if(novaBicicleta.getStatus() != Status.NOVA)
            throw new DataIntegratyViolationException("Bicicleta com status inválido para registro");
        return bicicletaRepository.save(mapper.map(novaBicicleta, Bicicleta.class));
    }

    @Override
    public Bicicleta atualizaBicicleta(NovaBicicleta novaBicicleta) {
        Bicicleta bicicleta = recuperaBicicleta(novaBicicleta.getId());
        mapper.map(novaBicicleta, bicicleta);
        return bicicletaRepository.save(bicicleta);
    }

    @Override
    public void deletaBicicleta(Long id) {
        Bicicleta bicicleta = recuperaBicicleta(id);
        if(bicicleta.getStatus() != Status.APOSENTADA)
            throw new DataIntegratyViolationException("Bicicleta não está em condições de remoção.");
        bicicletaRepository.delete(bicicleta);
    }

    @Override
    public void integrarNaRedeDeTotens(IntegraBicicletaDto integraBicicletaDto) {
        Bicicleta bicicleta = recuperaBicicleta(integraBicicletaDto.getIdBicicleta());

        Status statusBicicleta = bicicleta.getStatus();

        if( ! (statusBicicleta == Status.NOVA || statusBicicleta == Status.EM_REPARO) )
            throw new DataIntegratyViolationException("Bicicleta com status inválido.");

        Tranca tranca = trancaService.recuperaTranca(integraBicicletaDto.getIdTranca());
        trancaService.trancar(tranca, bicicleta);

        bicicleta.setStatus(Status.DISPONIVEL);

        bicicletaRepository.saveAndFlush(bicicleta);

        // Enviar email
        final Long idFuncionario = 1L;
        String emailFuncionario = externoService.getEmailFuncionario(idFuncionario);
        String mensagem = mensagemEmailBicicletaIntegrada(bicicleta, tranca);
        externoService.enviarEmail(emailFuncionario, mensagem);
    }

    private String mensagemEmailBicicletaIntegrada(Bicicleta bicicleta, Tranca tranca) {
        return "A bicicleta de id '"+ bicicleta.getId() +"' foi integrada a tranca de id '"+ tranca.getId() +"' com sucesso.";
    }

    @Override
    public void retirarDaRedeDeTotens(IntegraBicicletaDto integraBicicletaDto) {

        Bicicleta bicicleta = recuperaBicicleta(integraBicicletaDto.getIdBicicleta());
        Tranca tranca = trancaService.recuperaTranca(integraBicicletaDto.getIdTranca());

        if(! bicicleta.getId().equals(tranca.getBicicleta().getId()))
            throw new DataIntegratyViolationException("Bicicleta não está na tranca referenciada");

        if(bicicleta.getStatus() != Status.REPARO_SOLICITADO)
            throw new DataIntegratyViolationException("Bicicleta em estado inválido para operação");

        bicicleta = trancaService.destrancar(tranca);

        bicicleta.setStatus(Status.EM_REPARO);

        bicicletaRepository.saveAndFlush(bicicleta);

        // Enviar email
        final Long idFuncionario = 1L;
        String emailFuncionario = externoService.getEmailFuncionario(idFuncionario);
        String mensagem = mensagemEmailBicicletaRetirada(bicicleta, tranca);
        externoService.enviarEmail(emailFuncionario, mensagem);
    }

    private String mensagemEmailBicicletaRetirada(Bicicleta bicicleta, Tranca tranca) {
        return "A bicicleta de id '"+ bicicleta.getId() +"' foi retirada a tranca de id '"+ tranca.getId() +"' com sucesso.";
    }

    @Override
    public Bicicleta mudarStatus(Long id, String acao) {
        Bicicleta bicicleta = recuperaBicicleta(id);
        bicicleta.setStatus(Status.selecionaStatus(acao));
        bicicletaRepository.saveAndFlush(bicicleta);
        return bicicleta;
    }
}
