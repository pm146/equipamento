package com.scb.equipamento.services.impl;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Totem;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.NovoTotem;
import com.scb.equipamento.repositories.TotemRepository;
import com.scb.equipamento.repositories.TrancaRepository;
import com.scb.equipamento.services.TotemService;
import com.scb.equipamento.services.exceptions.DataIntegratyViolationException;
import com.scb.equipamento.services.exceptions.ObjectNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TotemServiceImpl implements TotemService {

    @Autowired
    private ModelMapper mapper;
    @Autowired
    private TotemRepository totemRepository;
    @Autowired
    private TrancaRepository trancaRepository;

    @Override
    public List<Totem> recuperaTotens() {
        return totemRepository.findAll();
    }

    @Override
    public Totem cadastraTotem(NovoTotem novoTotem) {
        verificaLocalizacao(novoTotem);
        return totemRepository.save(mapper.map(novoTotem, Totem.class));
    }

    @Override
    public Totem atualizaTotem(NovoTotem novoTotem) {
        recuperaTotem(novoTotem.getId());
        verificaLocalizacao(novoTotem);
        return totemRepository.save(mapper.map(novoTotem, Totem.class));
    }

    @Override
    public void deletaTotem(Long id) {
        Totem totem = recuperaTotem(id);
        totemRepository.delete(totem);
    }

    @Override
    public List<Tranca> recuperaTrancasEmTotem(Long id) {
        return recuperaTotem(id).getTrancas();
    }

    @Override
    public List<Bicicleta> recuperaBicicletasEmTotem(Long id) {
        return recuperaTrancasEmTotem(id)
                .stream().map(Tranca::getBicicleta).toList();
    }

    public Totem recuperaTotem(Long id) {
        Optional<Totem> totem = totemRepository.findById(id);
        return totem.orElseThrow( () -> new ObjectNotFoundException("Totem não encontrado") );
    }

    public void verificaLocalizacao(NovoTotem novoTotem){
        Optional<Totem> totem = totemRepository.findByLocalizacao(novoTotem.getLocalizacao());
        if(totem.isPresent())
            throw new DataIntegratyViolationException("Localização já cadastrada no sistema.");
    }
}

