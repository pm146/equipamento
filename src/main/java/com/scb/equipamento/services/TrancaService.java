package com.scb.equipamento.services;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.IntegraTrancaDto;
import com.scb.equipamento.models.dtos.TrancaBicicletaDto;
import com.scb.equipamento.models.dtos.NovaTranca;

import java.util.List;

public interface TrancaService {

    List<Tranca> recuperaTrancas();
    Tranca recuperaTranca(Long id);
    Tranca cadastraTranca(NovaTranca novaTranca);
    Tranca atualizaTranca(NovaTranca novaTranca);
    void deletaTranca(Long id);
    void integrarNaRedeDeTotens(IntegraTrancaDto integraTrancaDto);
    void retirarDaRedeDeTotens(IntegraTrancaDto integraTrancaDto);
    Bicicleta recuperaBicicletaEmTranca(Long id);
    void trancar(Tranca tranca, Bicicleta bicicleta);
    Tranca trancar(Long id, TrancaBicicletaDto trancaBicicletaDto);
    Bicicleta destrancar(Tranca tranca);
    Tranca destrancar(Long id, TrancaBicicletaDto trancaBicicletaDto);
    Tranca mudarStatus(Long id, String acao);
}
