package com.scb.equipamento.services;


import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Totem;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.NovoTotem;

import java.util.List;

public interface TotemService {

    Integer MAX_TRANCAS = 20;
    List<Totem> recuperaTotens();
    Totem cadastraTotem(NovoTotem novoTotem);
    Totem atualizaTotem(NovoTotem novoTotem);
    void deletaTotem(Long id);
    Totem recuperaTotem(Long id);
    List<Tranca> recuperaTrancasEmTotem(Long id);
    List<Bicicleta> recuperaBicicletasEmTotem(Long id);
}
