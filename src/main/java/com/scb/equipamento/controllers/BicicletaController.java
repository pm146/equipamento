package com.scb.equipamento.controllers;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.dtos.NovaBicicleta;
import com.scb.equipamento.models.dtos.IntegraBicicletaDto;
import com.scb.equipamento.services.BicicletaService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/bicicleta")
public class BicicletaController {

    private static final String ID = "/{id}";
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private BicicletaService bicicletaService;

    @GetMapping
    public ResponseEntity<List<Bicicleta>> recuperaBicicletas() {
        return ResponseEntity.ok().body( bicicletaService.recuperaBicicletas() );
    }

    @GetMapping(ID)
    public ResponseEntity<Bicicleta> recuperaBicicleta(@PathVariable Long id) {
        return ResponseEntity.ok().body( bicicletaService.recuperaBicicleta(id) );
    }

    @PostMapping
    public ResponseEntity<Bicicleta> cadastraBicicleta(@Valid @RequestBody NovaBicicleta novaBicicleta) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path(ID)
                .buildAndExpand(bicicletaService.cadastraBicicleta(novaBicicleta).getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(ID)
    public ResponseEntity<Bicicleta> atualizaBicicleta(@PathVariable Long id, @Valid @RequestBody NovaBicicleta novaBicicleta) {
        novaBicicleta.setId(id);
        return ResponseEntity.ok().body(bicicletaService.atualizaBicicleta(novaBicicleta));
    }

    @DeleteMapping(ID)
    public ResponseEntity<Bicicleta> deletaBicicleta(@PathVariable Long id) {
        bicicletaService.deletaBicicleta(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/integrarNaRede")
    public ResponseEntity<Bicicleta> integrarNaRedeDeTotens(@Valid @RequestBody IntegraBicicletaDto integraBicicletaDto) {
        bicicletaService.integrarNaRedeDeTotens(integraBicicletaDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/retirarDaRede")
    public ResponseEntity<Bicicleta> retirarDaRedeDeTotens(@Valid @RequestBody IntegraBicicletaDto integraBicicletaDto) {
        bicicletaService.retirarDaRedeDeTotens(integraBicicletaDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping(ID + "/status/{acao}")
    public ResponseEntity<Bicicleta> mudarStatus(@PathVariable Long id, @PathVariable String acao) {
        return ResponseEntity.ok().body(bicicletaService.mudarStatus(id, acao));
    }
}
