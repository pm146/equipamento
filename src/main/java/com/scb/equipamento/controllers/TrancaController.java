package com.scb.equipamento.controllers;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.IntegraTrancaDto;
import com.scb.equipamento.models.dtos.TrancaBicicletaDto;
import com.scb.equipamento.models.dtos.NovaTranca;
import com.scb.equipamento.services.TrancaService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/tranca")
public class TrancaController {

    public static final String ID = "/{id}";
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private TrancaService trancaService;

    @GetMapping
    public ResponseEntity<List<Tranca>> recuperaTrancas() {
        return ResponseEntity.ok().body(trancaService.recuperaTrancas());
    }

    @GetMapping(ID)
    public ResponseEntity<Tranca> recuperaTranca(@PathVariable Long id) {
        return ResponseEntity.ok().body(trancaService.recuperaTranca(id));
    }

    @PostMapping
    public ResponseEntity<Tranca> cadastraTranca(@Valid @RequestBody NovaTranca novaTranca) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path(ID)
                .buildAndExpand(trancaService.cadastraTranca(novaTranca).getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(ID)
    public ResponseEntity<Tranca> atualizaTranca(@PathVariable Long id, @Valid @RequestBody NovaTranca novaTranca) {
        novaTranca.setId(id);
        return ResponseEntity.ok().body(trancaService.atualizaTranca(novaTranca));
    }

    @DeleteMapping(ID)
    public ResponseEntity<Tranca> deletaTranca(@PathVariable Long id) {
        trancaService.deletaTranca(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/integrarNaRede")
    public ResponseEntity<Tranca> integrarNaRedeDeTotens(@Valid @RequestBody IntegraTrancaDto integraTrancaDto) {
        trancaService.integrarNaRedeDeTotens(integraTrancaDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/retirarDaRede")
    public ResponseEntity<Tranca> retirarDaRedeDeTotens(@Valid @RequestBody IntegraTrancaDto integraTrancaDto) {
        trancaService.retirarDaRedeDeTotens(integraTrancaDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping(ID + "/bicicleta")
    public ResponseEntity<Bicicleta> recuperaBicicletaEmTranca(@PathVariable Long id) {
        return ResponseEntity.ok().body(trancaService.recuperaBicicletaEmTranca(id));
    }

    @PostMapping(ID + "/trancar")
    public ResponseEntity<Tranca> trancar(@PathVariable Long id, @RequestBody TrancaBicicletaDto trancaBicicletaDto) {
        return ResponseEntity.ok().body(trancaService.trancar(id, trancaBicicletaDto));
    }

    @PostMapping(ID + "/destrancar")
    public ResponseEntity<Tranca> destrancar(@PathVariable Long id, @RequestBody TrancaBicicletaDto trancaBicicletaDto) {
        return ResponseEntity.ok().body(trancaService.destrancar(id, trancaBicicletaDto));
    }

    @PostMapping(ID + "/status/{acao}")
    public ResponseEntity<Tranca> mudarStatus(@PathVariable Long id, @PathVariable String acao) {
        return ResponseEntity.ok().body(trancaService.mudarStatus(id, acao));
    }
}

