package com.scb.equipamento.controllers;

import com.scb.equipamento.models.Bicicleta;
import com.scb.equipamento.models.Totem;
import com.scb.equipamento.models.Tranca;
import com.scb.equipamento.models.dtos.NovoTotem;
import com.scb.equipamento.repositories.TotemRepository;
import com.scb.equipamento.services.TotemService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/totem")
public class TotemController {

    public static final String ID = "/{id}";
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private TotemService totemService;
    @Autowired
    private TotemRepository totemRepository;

    @GetMapping
    public ResponseEntity<List<Totem>> recuperaTotens() {
        return ResponseEntity.ok().body(totemService.recuperaTotens());
    }

    @PostMapping
    public ResponseEntity<Totem> cadastraTotem(@Valid @RequestBody NovoTotem novoTotem) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path(ID)
                .buildAndExpand(totemService.cadastraTotem(novoTotem).getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(ID)
    public ResponseEntity<Totem> atualizaTotem(@PathVariable Long id, @Valid @RequestBody NovoTotem novoTotem) {
        novoTotem.setId(id);
        return ResponseEntity.ok().body(totemService.atualizaTotem(novoTotem));
    }

    @DeleteMapping(ID)
    public ResponseEntity<Totem> deletaTotem(@PathVariable Long id) {
        totemService.deletaTotem(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(ID + "/trancas")
    public ResponseEntity<List<Tranca>> recuperaTrancasEmTotem(@PathVariable Long id) {
        return ResponseEntity.ok().body(totemService.recuperaTrancasEmTotem(id));
    }

    @GetMapping(ID + "/bicicletas")
    public ResponseEntity<List<Bicicleta>> recuperaBicicletasEmTotem(@PathVariable Long id) {
        return ResponseEntity.ok().body(totemService.recuperaBicicletasEmTotem(id));
    }
}
